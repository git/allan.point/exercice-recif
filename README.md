# Exercice-RÉCIF

## Sujet

Réaliser un script Python qui, à partir de l'API de la Flotte Océanographique Française, permet d'identifier :
* le navire ayant été le plus au nord en 2021
* le navire dont la moyenne des relevés de température d'eau est la plus élevée en 2021

## Utilistation

### Cloner le dépot

```SH
git clone https://gitlab.iut-clermont.uca.fr/alpoint/exercice-recif.git && cd exercice-recif || echo Clonning error
```

### Lancer le programme

* Sur GNU/Linux

```SH
python3 src/main.py 2021
```
*Il est possible de choisir la date que l'on veut, tant que c'est un entier*

* Sur Windows

```SH
python src/main.py 2021
```
*Il est possible de choisir la date que l'on veut, tant que c'est un entier*

## Tests

Pour lancer les tests unitaires, il faut se placer à la racine du projet (le dossier exercice-recif) suivre les différentes étapes:

```SH
git checkout tests
cd tests
python3 main.py
```
