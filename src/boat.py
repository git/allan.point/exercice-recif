#coding:utf-8
import statistics
from custom_exception.nothing_to_compare_error import *



class Boat:
    """
    Representation of a boat.
    A boat is defined by its name, ids, url, avatar, temperatures ans positions in the sea.
    """
    def __init__(self, boat_id: str, name: str, sismer_id: str, url: str, avatar:str , temperatures: list, positions: list):
        self.boat_id = boat_id
        self.name = name
        self.sismer_id = sismer_id
        self.url = url
        self.avatar = avatar
        self.temperatures = temperatures
        self.positions = positions
    def __str__(self):
        """
        When a Boat goes to string, the returned value is simply the name of this Boat.
        """
        return self.name
    def __eq__(self, other):
            if other is None:
                return False
            if not isinstance(other, self.__class__):
                return False
            return self.boat_id == other.boat_id

    def get_temp_mean(self):
        """
        Process the average of temperatures with None values removed.
        """
        return statistics.mean(list(filter(lambda v: v is not None, self.temperatures)))
    def get_northiest_coords(self):
        """
        From all positions send by the boat, return the one on the northiest.
        Will raise a NothingToCompareError if the boat has no position saved
        """
        if len(self.positions) == 0:
            raise NothingToCompareError(f"{self.name} must have coords to compare")
        best = self.positions[0]
        for position in self.positions:
            if position.is_norther_than(best):
                best = position
        return best
                
    def print_details(self):
        """
        Print the Boat with more details than __str__
        """
        print(f"[{self.name}({self.boat_id})]\nSISMER ID: {self.sismer_id}\nURL: {self.url}\nAvatar: {self.avatar}")
