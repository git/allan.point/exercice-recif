#coding:utf-8

from statistics import StatisticsError
from boats_loader import BoatsLoader
from boat import Boat
from position import Position
from custom_exception.nothing_to_compare_error import NothingToCompareError

class Fleet:
    """
    Contains all boats and can compare eache one
    """
    def __init__(self, year: int, loader :BoatsLoader):
        self.boats = loader.load_boats(year)
        

    def compare_boats(self, getter: callable, comparator: callable):
        """
        Compare each boat with dynamic parameter
        getter: This callable must be a methode from Boat and must have no parameters. It will allow to select data to compare.
        comparator: This callable must have its twp parameters's type the same that the one return by getter. It will allow to compare data each other
        """
        if len(self.boats) == 0:
            raise NothingToCompareError("No boats to compare")
        best_boat = None
        best_element_to_compare = None
        for boat in self.boats:
            try:
                data = getter(boat)
                if comparator(data, best_element_to_compare):
                    best_boat = boat
                    best_element_to_compare = data
            except StatisticsError as e:
                continue
            except NothingToCompareError as e:
                print(e)
        return best_boat, best_element_to_compare

    def get_northiest(self):
        """
        Returns the boat which went the northiest and its coords.
        """
        return self.compare_boats(Boat.get_northiest_coords, Position.is_norther_than)
    def get_highiest_heat_mean(self):
        """
        Returns the boat which saved the highiest heat average and this average.
        """
        return self.compare_boats(Boat.get_temp_mean, float.__ge__)
        

