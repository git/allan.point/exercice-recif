#! encoding: utf-8

from sys import argv
import fleet
from rest_api_boats_loader import RestApiBoatsLoader

# If the year give by user. if not print a message and exit
if len(argv) <= 1:
    print("You must give the year you want to get (eg. python3 main.py 2021)")
    exit(1)

# Is the argument given a number. If not print a message and exit
try:
    year = int(argv[1])
except ValueError:
    print("The year must be a number")
    exit(1)

# fleet loading with data from the year
my_fleet = fleet.Fleet(year, RestApiBoatsLoader())

# Print detail for each boat from the fleet
for boat in my_fleet.boats:
    boat.print_details()
    print()

# get the boat which went the northiest
northiest_boat, northiest_coords = my_fleet.get_northiest()

# get the boat which have the highiest heat average saved
hottiest_boat, heat_mean = my_fleet.get_highiest_heat_mean()

# Prints results
if northiest_boat is None:
    print(f"No data to find the northiest boat for {year}")
else:
    print(f"The boat which went northiest is the {northiest_boat}. It went in {northiest_coords}")

if hottiest_boat is None:
    print(f"No data to find the boat which saved the hottiest heat for {year}")
else:
    print(f"The boat which saved heighiest heat is the {hottiest_boat}. It saved an average of {round(heat_mean, 2)}°C")
