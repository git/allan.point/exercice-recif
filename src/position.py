#coding:utf-8

class Position:
    """
    Position's representation: [<latitude>; <longitude>]
    """
    def __init__(self, latitude: float, longitude: float):
        self.latitude = latitude
        self.longitude = longitude

    def __str__(self):
        return f"[{self.latitude}; {self.longitude}]"
    def is_norther_than(self, position):
        """
        Is the current instance of postition norther than <postition>
        """
        if position == None:
            return True
        return self.latitude >= position.latitude
    def __eq__(self, other):
        if other is None:
            return False
        if not isinstance(other, self.__class__):
            return False
        return self.longitude == other.longitude and self.latitude == other.latitude

