#coding:utf-8

from boat import Boat
from position import Position
from boats_loader import BoatsLoader
import requests

class RestApiBoatsLoader(BoatsLoader):
    """
    Allows to load Boats from https://localisation.flotteoceanographique.fr/api/v2/vessels
    """
    def __init__(self):
        self.url = 'https://localisation.flotteoceanographique.fr/api/v2/vessels'
        self.search_begin = '-01-01T00:00:00.000Z'
        self.search_end = '-12-31T23:59:59.000Z'

    
    def load_boats(self, year: int):
        """
        Create and return Boats with Loaded data fom flotteoceanographique.fr
        """
        raw_data = self.load_from_url(self.url)
        boats = []
        for boat in raw_data.json():
            boat_data = self._load_boat_data(boat['id'], year)
            tmp_boat = Boat(boat['id'], boat['name'], boat['sismerId'], boat['url'], boat['avatar'], boat_data['temp'], boat_data['pos'])
            boats.append(tmp_boat)
        return boats

    def _load_boat_data(self, boat_id: str, year: int):
        """
        Collects all positions and sea temperatues send by <boat_id> in <year>.
        Return a Dictionary with 2 keys: 'pos' and 'temp'
        """
        json_data = self.load_from_url(self.url + f"/{boat_id}/positions?startDate={year}{self.search_begin}&endDate={year}{self.search_end}").json()
        data = {}
        data['pos'] = []
        data['temp'] = []
        for single_data in json_data:
            if len(single_data['data']) == 0:
                continue
            data['pos'].append(Position(single_data['lat'], single_data['lon']))

            if 'seatemp' in single_data['data']:
                data['temp'].append(single_data['data']['seatemp'])
        return data
    
    def load_from_url(self, url: str):
        """
        Send a GET request to <url> and convert result in Response (see requests module)
        """
        data = requests.get(url)
        if(data.status_code != 200):
            raise custom_exception.HttpException(f"Error {data.status_code}")
        return data
